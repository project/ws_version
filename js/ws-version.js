(function ($) {
  Drupal.behaviors.wsVersion = {
    attach: function () {
      $('#toolbar-bar').append("<div style='padding: 1em 1.3333em; line-height: 1em; float: right;' class='ws-version toolbar-tab'>" + drupalSettings.version_content + "</div>");
    }}
})(jQuery);
